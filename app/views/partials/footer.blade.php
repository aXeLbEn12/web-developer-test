<?php

// For now build the menu manually
// TODO: Build a class or data store for menu items, possibly use laravel administrator for public facing end
$footerMenu = array(
    (object) array('name' => 'Members', 'href' => '/members'),
    (object) array('name' => 'Blog', 'href' => '/blog'),
    (object) array('name' => 'FAQ', 'href' => '/faq'),
    (object) array('name' => 'Privacy Policy', 'href' => '/privacy-policy'),
    (object) array('name' => 'Terms of Use', 'href' => '/terms-of-use'),
    (object) array('name' => 'Contact Us', 'href' => '/contact-us'),
);

// TODO: Create alternative phone-dropdown menu
?>
<footer>
    <nav class="row">
        <ul>
            @foreach ($footerMenu as $menuItem)
            <li><a href="{{ url($menuItem->href) }}">{{ $menuItem->name }}</a></li>
            @endforeach
        </ul>
        <div class="text-center">
            <small>Copyright {{ date('Y') }} &copy; Many Amigos</small>
        </div>
    </nav>
</footer>