@extends('layouts.public')

@section('content')


	<div class="row" id="ContactUs">
		<h2>Contact Us</h2>
		<hr />
		<?php
			if ( Session::get('message') ) {
				echo '<p class="message">'.Session::get('message').'</p>';
			}
		?>
	
		{{Form::open(array('url'=>'contact-us', 'method'=>'post', 'id'=>'contactForm'))}}
			<div class="large-9">
				<div class="row">
					<div class="large-6 columns">{{Form::label('First_Name', 'First Name *', array('class' => 'right'))}}</div>
					<div class="large-6 columns">
						{{Form::text('First_Name', Input::old('First_Name'), array('placeholder' => 'First Name', 'required'));}}
						@if ( $errors->has('First_Name'))
							<p class="error-msg-specific"><?php echo $errors->first('First_Name'); ?></p>
						@endif
					</div>
				</div> <!-- row end -->
			</div> <!-- large-9 end -->
			
			<div class="large-9">
				<div class="row">
					<div class="large-6 columns">{{Form::label('Last_Name', 'Surname *', array('class' => 'right'))}}</div>
					<div class="large-6 columns">
						{{Form::text('Last_Name', Input::old('Last_Name'), array('placeholder' => 'Last Name', 'required'));}}
						@if ($errors->has('Last_Name'))
						<p class="error-msg-specific"><?php echo $errors->first('Last_Name'); ?></p>
						@endif
					</div>
				</div> <!-- row end -->
			</div> <!-- large-9 end -->
			
			<div class="large-9">
				<div class="row">
					<div class="large-6 columns">{{Form::label('Email_Address', 'Email Address *', array('class' => 'right'))}}</div>
					<div class="large-6 columns">
						{{Form::text('Email_Address', Input::old('Email_Address'), array('placeholder' => 'Email Address', 'required'));}}
						@if ($errors->has('Email_Address'))
						<p class="error-msg-specific"><?php echo $errors->first('Email_Address'); ?></p>
						@endif
					</div>
				</div> <!-- row end -->
			</div> <!-- large-9 end -->
			
			<div class="large-9">
				<div class="row">
					<div class="large-6 columns">{{Form::label('Contact_Number', 'Daytime contact number *', array('class' => 'right'))}}</div>
					<div class="large-6 columns">
						{{Form::text('Contact_Number', Input::old('Contact_Number'), array('placeholder' => 'Daytime contact number', 'required'));}}
						@if ($errors->has('Contact_Number'))
						<p class="error-msg-specific"><?php echo $errors->first('Contact_Number'); ?></p>
						@endif
					</div>
				</div> <!-- row end -->
			</div> <!-- large-9 end -->
			
			<div class="large-9">
				<div class="row">
					<div class="large-6 columns">{{Form::label('Address', 'Address *', array('class' => 'right'))}}</div>
					<div class="large-6 columns">
						{{Form::text('Address', Input::old('Address'), array('placeholder' => 'Address', 'required'));}}
						@if ($errors->has('Address'))
						<p class="error-msg-specific"><?php echo $errors->first('Address'); ?></p>
						@endif
					</div>
				</div> <!-- row end -->
			</div> <!-- large-9 end -->
			
			<div class="large-9">
				<div class="row">
					<div class="large-6 columns">{{Form::label('Suburb', 'Suburb *', array('class' => 'right'))}}</div>
					<div class="large-6 columns">
						{{Form::text('Suburb', Input::old('Suburb'), array('placeholder' => 'Suburb', 'required'));}}
						@if ($errors->has('Suburb'))
						<p class="error-msg-specific"><?php echo $errors->first('Suburb'); ?></p>
						@endif
					</div>
				</div> <!-- row end -->
			</div> <!-- large-9 end -->
			
			<div class="large-9">
				<div class="row">
					<div class="large-6 columns">{{Form::label('States', 'States *', array('class' => 'right'))}}</div>
					<div class="large-6 columns">
						{{Form::select('States', $states, Input::old('States'), array('required'));}}
						@if ($errors->has('States'))
						<p class="error-msg-specific"><?php echo $errors->first('States'); ?></p>
						@endif
					</div>
				</div> <!-- row end -->
			</div> <!-- large-9 end -->
			
			<div class="large-9">
				<div class="row">
					<div class="large-6 columns">{{Form::label('Post_Code', 'Post Code *', array('class' => 'right'))}}</div>
					<div class="large-6 columns">
						{{Form::text('Post_Code', Input::old('Post_Code'), array('placeholder' => 'Post Code', 'required'));}}
						@if ($errors->has('Post_Code'))
						<p class="error-msg-specific"><?php echo $errors->first('Post_Code'); ?></p>
						@endif
					</div>
				</div> <!-- row end -->
			</div> <!-- large-9 end -->
			
			<div class="large-9">
				<div class="row">
					<div class="large-6 columns">{{Form::label('Enquiry_Type', 'Enquiry Type *', array('class' => 'right'))}}</div>
					<div class="large-6 columns">
						{{Form::select('Enquiry_Type', $enquiry_type, Input::old('Enquiry_Type'), array('required'))}}
						@if ($errors->has('Enquiry_Type'))
						<p class="error-msg-specific"><?php echo $errors->first('Enquiry_Type'); ?></p>
						@endif
					</div>
				</div> <!-- row end -->
			</div> <!-- large-9 end -->
			
			<div class="large-9">
				<div class="row">
					<div class="large-6 columns">{{Form::label('Product_Name', 'Product Name', array('class' => 'right'))}}</div>
					<div class="large-6 columns">
						{{Form::text('Product_Name', Input::old('Product_Name'), array('placeholder' => 'Product Name'));}}
						@if ($errors->has('Product_Name'))
						<p class="error-msg-specific"><?php echo $errors->first('Product_Name'); ?></p>
						@endif
					</div>
				</div> <!-- row end -->
			</div> <!-- large-9 end -->
			
			<div class="large-9">
				<div class="row">
					<div class="large-6 columns">{{Form::label('Product_Size', 'Product Size', array('class' => 'right'))}}</div>
					<div class="large-6 columns">
						{{Form::text('Product_Size', Input::old('Product_Size'), array('placeholder' => 'Product Size'));}}
						@if ($errors->has('Product_Size'))
						<p class="error-msg-specific"><?php echo $errors->first('Product_Size'); ?></p>
						@endif
					</div>
				</div> <!-- row end -->
			</div> <!-- large-9 end -->
			
			<div class="large-9">
				<div class="row">
					<div class="large-6 columns">{{Form::label('Use_By_Date', 'Use by Date', array('class' => 'right'))}}</div>
					<div class="large-6 columns">
						{{Form::text('Use_By_Date', Input::old('Use_By_Date'), array('placeholder' => 'Use by Date'));}}
						@if ($errors->has('Use_By_Date'))
						<p class="error-msg-specific"><?php echo $errors->first('Use_By_Date'); ?></p>
						@endif
					</div>
				</div> <!-- row end -->
			</div> <!-- large-9 end -->
			
			<div class="large-9">
				<div class="row">
					<div class="large-6 columns">{{Form::label('Batch_Code', 'Batch Code', array('class' => 'right'))}}</div>
					<div class="large-6 columns">
						{{Form::text('Batch_Code', Input::old('Batch_Code'), array('placeholder' => 'Batch Code'));}}
						@if ($errors->has('Batch_Code'))
						<p class="error-msg-specific"><?php echo $errors->first('Batch_Code'); ?></p>
						@endif
					</div>
				</div> <!-- row end -->
			</div> <!-- large-9 end -->
			
			<div class="large-9">
				<div class="row">
					<div class="large-6 columns">{{Form::label('Enquiry', 'Enquiry', array('class' => 'right'));}}</div>
					<div class="large-6 columns">{{Form::textarea('Enquiry', Input::old('Enquiry'), array('placeholder' => 'Enquiry'));}}</div>
				</div> <!-- row end -->
			</div> <!-- large-9 end -->
			
			<div class="large-9">
				<div class="row">
					<div class="large-6 columns">&nbsp;</div>
					<div class="large-6 columns">
						{{Form::submit('Submit', array('id' => 'submitbutton'))}}
					</div>
				</div> <!-- row end -->
			</div> <!-- large-9 end -->
		{{Form::close();}}
	</div> <!-- contactus end -->


@stop