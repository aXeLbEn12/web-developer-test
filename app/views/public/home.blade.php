@extends('layouts.public')

@section('content')

    <div class="row">
        <section class="small-12 columns home-top-bar">
            <h2 class="text-center subtitle">
                {{ Lang::get('home.tagline') }}
            </h2>
        </section>
    </div>

    <div class="row">
        <article class="home-call-to-action clearfix">
            <section class="medium-7 large-7 columns">
                <div class="home-video radius hidden-for-small">
                    <i class="home-video-icon fi-play-circle"></i>
                </div>
            </section>
            <section class="medium-5 large-5 columns text-center">
                <h1 class="home-logo-wrapper">
                    <span class="far-title">
                        {{ Lang::get('home.heading') }}
                    </span>
                </h1>
                <div class="home-buttons">
                    <a href="{{ url('sign-up') }}" class="button expand radius">Sign up</a>
                    <br>
                    <span class="text-light">or</span>
                    <a href="#" data-reveal-id="sign-in-modal">Sign in</a>
                </div>
            </section>
            <section class="small-12 columns">
                <div class="home-video radius visible-for-small">
                    <i class="home-video-icon fi-play-circle"></i>
                </div>
            </section>
        </article>
    </div>

    <div class="row home-separator text-center">
        <hr>
        <p class="text-light">Learn more <i class="fi-link"></i></p>
    </div>

    <div class="row">

        <article class="home-summary clearfix">
            <section class="medium-7 columns">
                <h3>{{ Lang::get('home.raise-money-heading') }}</h3>
                <p>
                    {{ Lang::get('home.raise-money-copy') }}
                </p>
                <p class="text-right">
                    <a href="{{ url('how-it-works/raising-money') }}" class="button small secondary radius">Learn more</a>
                </p>
            </section>
            <section class="medium-5 columns text-center hide-for-small">
                @if (Lang::has('home.raise-money-image'))
                    {{ HTML::image(url('img/' . Lang::get('home.raise-money-image')), 'How to raise money', array('class' => 'image-circle')) }}
                @endif
            </section>
        </article>

        <article class="home-summary clearfix">
            <section class="medium-5 columns text-center hide-for-small">
                @if (Lang::has('home.investing-image'))
                    {{ HTML::image(url('img/' . Lang::get('home.investing-image')), 'How to invest money', array('class' => 'image-circle')) }}
                @endif
            </section>
            <section class="medium-7 columns">
                <h3>{{ Lang::get('home.investing-heading') }}</h3>
                <p>
                    {{ Lang::get('home.investing-copy') }}
                </p>
                <p class="text-right">
                    <a href="{{ url('how-it-works/investing') }}" class="button small secondary radius">Learn more</a>
                </p>
            </section>
        </article>

    </div>

    <div class="row home-separator text-center">
    </div>

@stop