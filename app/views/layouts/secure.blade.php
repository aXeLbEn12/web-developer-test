@extends('layouts._wrapper')

@section('layout')

    <div class="application-wrapper">
        @yield('content')
    </div>

@stop