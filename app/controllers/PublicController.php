<?php

class PublicController extends BaseController
{

    /**
     * Home Page
     *
     * @return mixed
     */
    public function showHomePage()
    {
        $viewItems = $this->getAllMetaData('home');
        $viewItems['hideMenu'] = true;

        return View::make('public.home', $viewItems);
    }

    /**
     * How it Works Page
     *
     * @return mixed
     */
    public function showHowItWorksPage()
    {
        $viewItems = $this->getAllMetaData('how-it-works');
        $viewItems['pageTitle'] = 'How it Works';

        return View::make('public.how-it-works', $viewItems);
    }

    /**
     * How it Works: Raising Money
     *
     * @return mixed
     */
    public function showRaisingMoneyPage()
    {
        $viewItems = array(
            'mainTitle' => 'How it Works: Raising Money',
            'pageTitle' => 'Raising Money'
        );
        return View::make('public.raising-money', $viewItems);
    }

    /**
     * How it Works: Investing
     *
     * @return mixed
     */
    public function showInvestingPage()
    {
        $viewItems = array(
            'mainTitle' => 'How it Works: Investing',
            'pageTitle' => 'Investing'
        );
        return View::make('public.investing', $viewItems);
    }

    /**
     * Privacy Policy
     *
     * @return mixed
     */
    public function showPrivacyPolicyPage()
    {
        $viewItems = array(
            'pageTitle' => 'Privacy Policy'
        );
        return View::make('public.privacy-policy', $viewItems);
    }

    /**
     * FAQ Page
     *
     * @return mixed
     */
    public function showFaqPage()
    {
        $viewItems = array(
            'pageTitle' => 'Frequently Asked Questions'
        );
        return View::make('public.faq', $viewItems);
    }

    /**
     * Terms of Use Page
     * @return mixed
     */
    public function showTermsOfUsePage()
    {
        $viewItems = array(
            'pageTitle' => 'Terms of Use'
        );
        return View::make('public.terms-of-use', $viewItems);
    }

    /**
     * Contact Page
     *
     * @return mixed
     */
    public function showContactPage()
    {
        $viewItems = array(
            'pageTitle' => 'Contact Us'
        );
        
        $viewItems = array();
        
        // put this in a database/json file if needed, but for the moment, just use arrays
        $viewItems['states'] = array (
            '' => '--- Select ---',
            'ACT' => 'ACT',
            'NSW' => 'NSW',
            'NT' => 'NT',
            'QLD' => 'QLD',
            'SA' => 'SA',
            'TAS' => 'TAS',
            'VIC' => 'VIC',
            'WA' => 'WA'
        );
        $viewItems['enquiry_type'] =  array(
            '' => '--- Select ---',
            1 => 'General enquiry',
            2 => 'Product feedback or enquiry',
            3 => 'Product complaint'
        );
        return View::make('public.contact', $viewItems);
    }

    /**
     * Creates an array with the available meta data for a view
     *
     * @param $view
     * @return array
     */
    private function getAllMetaData($view)
    {
        return array(

            'metaTitle'     => (Lang::has("{$view}.meta-title"))
                ? Lang::get("{$view}.meta-title")
                : null,

            'metaDescription' => (Lang::has("{$view}.meta-description"))
                ? Lang::get("{$view}.meta-description")
                : null,

            'metaKeywords'  => (Lang::has("{$view}.meta-keywords"))
                ? Lang::get(Lang::get("{$view}.meta-keywords"))
                : null,

            'ogTitle'       => (Lang::has("{$view}.og-title"))
                ? Lang::get("{$view}.og-title")
                : null,

            'ogDescription' => (Lang::has("{$view}.og-description"))
                ? Lang::get("{$view}.og-description")
                : null,

            'ogImage'       => (Lang::has("{$view}.og-image"))
                ? Lang::get("{$view}.og-image")
                : null,

        );
    }

}
