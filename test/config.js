/*(function(global) {
    global.monkeytestjs = {
        "local": {
            "env": ["localhost"]
        },
        "testsDir": "tests/mytests",
        "pages": [
            {
                "url": "../contact-us",
                "tests": [ "page/contactus_test.js" ]
            }
        ],
        "proxyUrl": "tests/core/proxy.php?mode=native&url=<%= url %>",
        "loadSources": true
    };
})(this);
*/
(function(global) {
    global.monkeytestjs = {
        "local": {
            "env": ["localhost"]
        },
        "testsDir": "mytests",
        "globalTests": [
            //"page/w3_validation_test.js"
        ],
        "pages": [
            {
                "url": "../public/contact-us",
                "tests": [ "page/required_contact_fields.js" ]
            }
           
        ],
        "proxyUrl": "core/proxy.php?mode=native&url=<%= url %>",
        "loadSources": true
    };
})(this);