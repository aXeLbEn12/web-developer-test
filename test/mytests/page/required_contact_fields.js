registerTest ('Demo page test', {
    setup: function() {
		this.requiredFields = ["First_Name", "Last_Name", "Email_Address", "Contact_Number", "Address", "Suburb", "States", "Post_Code", "Enquiry_Type"];
		this.requiredEnquiryFields = ['Product_Name', 'Product_Size', 'Use_By_Date', 'Batch_Code'];
		this.formName = "contactForm";
		this.submitbtnId = "submitbutton";
		this.errorClassName = "error-msg-specific";
    },
    load:function($) {
        var self = this;
		var requiredFields = self.requiredFields;
		var requiredEnquiryFields = self.requiredEnquiryFields;
		var doc = self.workspace.document;
		var errorField = self.errorClassName;
		
		// submit the contact form ...
		this.
		test("Submitting empty contact form...",function($) {
			ok(true, 'Contact field submitted for testing');
		}).waitForPageLoadAfter(function($) {
			var contactForm = self.workspace.window.$('#contactForm');
			contactForm.submit();
		});
		
		// testing submitted form with blank values
		this.test("Validate required fields. [with blank fields]",function($) {
			for ( var i=0; i<requiredFields.length; i++ ) {
				var currentField = self.workspace.window.$('#'+requiredFields[i]);
				var currentFieldError = currentField.next('.error-msg-specific');
				
				if ( currentField.val() == '' && currentFieldError.length > 0 ) {
					ok(true, requiredFields[i] + ' field is blank and an error message was found. PASSED');
				} else {
					ok(false, requiredFields[i] + ' field is blank and no error message was found. FAILED');
				}
			}
		});
		
		// testing Enquiry Type required fields
		this.test("Validate Enquiry Type required fields.[with blank fields]",function($) {
			var enquiryType = self.workspace.window.$('#Enquiry_Type');
			
			if ( enquiryType == 3 ) {
				for ( var j=0; j<requiredEnquiryFields.length; j++ ) {
					var currentField = self.workspace.window.$('#'+requiredEnquiryFields[j]);
					if ( currentField.val() == '' ) {
						ok(false, requiredEnquiryFields[j] + ' field is blank and no error message was found[Product Complaint is selected]. FAILED');
					}
				}
			} else {
				ok(true, 'Enquiry Type is not equal to Produt Complaint so Product Name, Product Size, Use by Date, and Batch code are not required.');
			}
		})
		
		// resubmit contact form, this time with populated contents
		this.
		test("Submitting populated contact form...",function($) {
			for ( var i=0; i<requiredFields.length; i++ ) {
				var self = this;
				var currentField = self.workspace.window.$('#'+requiredFields[i]);
				currentField.val("TEST CONTENT");
				
				ok(true, requiredFields[i]+' field populated for another set of testing.');
			}
		}).waitForPageLoadAfter(function($) {
			var contactForm = self.workspace.window.$('#contactForm');
			contactForm.submit();
		});
		
		// testing submitted form with test populated values
		this.test("Validate required fields. [with populated fields]",function($) {
			for ( var i=0; i<requiredFields.length; i++ ) {
				var currentField = self.workspace.window.$('#'+requiredFields[i]);
				var currentFieldError = currentField.next('.error-msg-specific');
				
				if ( currentField.val() == '' && currentFieldError.length == 0 ) {
					ok(true, requiredFields[i] + ' field is blank and an error message was found. PASSED');
				} else {
					ok(false, requiredFields[i] + ' field is blank and no error message was found. FAILED');
				}
			}
		}).test("Validate Enquiry Required fields", function ($) {
			var enquiryType = self.workspace.window.$('#Enquiry_Type');
			
			if ( enquiryType == 3 ) {
				for ( var j=0; j<requiredEnquiryFields.length; j++ ) {
					var currentField = self.workspace.window.$('#'+requiredEnquiryFields[j]);
					if ( currentField.val() == '' ) {
						ok(false, requiredEnquiryFields[j] + ' field is blank and no error message was found[Product Complaint is selected]. FAILED');
					}
				}
			} else {
				ok(true, 'Enquiry Type is not equal to Produt Complaint so Product Name, Product Size, Use by Date, and Batch code are not required.');
			}
		});

    }
});
