angular.module('manyAmigosClient').provider('UserManager', [function() {

    function UserManager($http) {
        this.$http    = $http;
        this.loggedIn = false;
    }

    /**
     * Authenticates a user
     * @param username
     * @param password
     */
    UserManager.prototype.authenticate = function(username, password) {
        return this.$http.post('/sign-in', {
            username: username,
            password: password
        });
    };

    this.$get = ['$http', function($http) {
        return new UserManager($http);
    }];
}]);